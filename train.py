#!/usr/bin/env python

from fastai.vision import *
from fastai.callbacks.hooks import *
from fastai.utils.mem import *
from glob import glob
from pathlib import Path
from pooch import retrieve
from skimage import util, io
from skimage.color import rgb2gray
from typing import Dict, Union

import argparse
import numpy as np


# defining global variables.
BASE_PATH = Path('.')
MODEL_ONLINE = 'https://gitlab.com/alexdesiqueira/butterfly-wings-data-unet/-/raw/master/unet_model/unet_butterfly.pkl'
HASH_ONLINE = 'https://gitlab.com/alexdesiqueira/butterfly-wings-data-unet/-/raw/master/unet_model/SHA256SUM'


def main() -> None:
    """Main function for train.py. Receives arguments and starts train()."""
    # creating parser and checking arguments.
    help_description = """Train an U-net network on butterfly images."""
    parser = argparse.ArgumentParser(description=help_description,
                                     add_help=True)

    # argument --weights.
    help_weights = """file containing weight training coefficients. Default:
                      'unet_model/unet_butterfly.pkl'"""
    parser.add_argument('-w',
                        '--weights',
                        type=str,
                        required=False,
                        help=help_weights)

    # argument --train_vars.
    help_train_vars = """JSON file containing the training and destination
                         folders, window shape and pad width. Defaults: values
                         based on previous training structure."""
    parser.add_argument('-t',
                        '--train_vars',
                        type=str,
                        required=False,
                        help=help_train_vars)

    # argument --batch_size.
    help_batch_size = """size of the batches used in the training (optional).
                         Default: 1"""
    parser.add_argument('-b',
                        '--batch_size',
                        type=int,
                        required=False,
                        help=help_batch_size)

    # argument --epochs.
    help_epochs = """how many epochs are used in the first and second training
                     cycles (optional).
                     Default: (25, 10)"""
    parser.add_argument('-e',
                        '--epochs',
                        type=list,
                        required=False,
                        help=help_epochs)

    # argument --learning_rate.
    help_lr = """learning rate (lr) used during training. The second
                 training cycle uses (lr/400, lr/4). Default: 1E-4"""
    parser.add_argument('-l',
                        '--learning_rate',
                        type=list,
                        required=False,
                        help=help_lr)

    arguments = vars(parser.parse_args())

    weights, train_vars, batch_size, epochs, learn_rate = list(arguments.values())
    # checking if batch_size and epochs are empty.
    if not batch_size:
        batch_size = 1
    if not epochs:
        epochs = (25, 10)

    # starting train().
    train(weights, train_vars, batch_size, epochs, learn_rate)

    return None


def train(weights: Union[str, None] = 'unet_model/unet_butterfly.pkl',
          train_vars: Union[str, None] = None,
          batch_size: int = 1,
          epochs: list = (25, 10),
          learn_rate: float = 1E-4):
    """Train an U-net to segment the butterflies.

    Parameters
    ----------
    weights : str or None (default : 'unet_model/unet_butterfly.pkl')
        Input containing weight coefficients. If None, checks for the default
        coefficients in `unet_model/unet_butterfly.pkl`.
    train_vars : str or None (default : None)
        JSON file containing the training and destination
        folders, window shape and pad width. If None, uses values based on
        previous training structure.
    batch_size : int (default : 1)
        Size of the batches used in the training.
    epochs : int (default : (25, 10))
        How many epochs are used in the first and second training cycles.
    learn_rate : float (default : 1E-4)
        Learning rate (lr) used during training. The second
        training cycle uses (lr/400, lr/4).

    Returns
    -------
    None
    """
    # checking if weights is empty or the last version.
    weights = _check_weights(weights)

    # checking if train_vars needs to come from constants.py or the JSON file.
    if train_vars:
        train_vars = _read_training_variables(filename=train_vars)
    else:
        train_vars = _training_variables()

    # defining folders.
    FOLDER_TRAIN = train_vars['folder_train']
    FOLDER_IMAGES = Path(FOLDER_TRAIN)/'images'
    FOLDER_LABELS = Path(FOLDER_TRAIN)/'labels'

    FOLDER_DUMP = train_vars['folder_dump']
    DUMP_IMAGES = Path(FOLDER_DUMP)/'images'
    DUMP_LABELS = Path(FOLDER_DUMP)/'labels'

    FOLDER_TEMP = '.tmp_training'
    TEMP_IMAGES = Path(FOLDER_TEMP)/'images'
    TEMP_LABELS = Path(FOLDER_TEMP)/'labels'

    size = train_vars['win_shape']
    win_shape_image = np.append(size, 3)  # necessary?

    # check if images and respective labels exist and have the same size.
    _check_images(FOLDER_IMAGES, FOLDER_LABELS)

    # reading images and training variables.
    image_names = get_image_files(FOLDER_IMAGES)
    label_names = get_image_files(FOLDER_LABELS)

    get_y_fn = lambda x: FOLDER_LABELS/f'{x.stem}{x.suffix}'

    codes = array(['background', 'butterfly'])

    src = (SegmentationItemList.from_folder(FOLDER_IMAGES, convert_mode='RGB')
           # Load in x data from folder
           .split_by_rand_pct(0.2)
           # Split data into training and validation set
           .label_from_func(get_y_fn,
                            classes=codes,
                            after_open=util.img_as_float32))
           # Label data using the get_y_fn function

    # data augmentation options.
    transforms = get_transforms(flip_vert=True,
                                max_rotate=15,
                                max_zoom=1.25,
                                max_lighting=0.3)

    data = (src.transform(transforms, size=size, tfm_y=True)
            .databunch(bs=batch_size)
            .normalize(imagenet_stats)
    )

    # adding measurements to check how training is performing.
    m_acc = partial(accuracy_thresh, thresh=0.2)
    m_fscore = partial(fbeta, thresh=0.2)
    m_dice = partial(dice)
    lr_stage_1 = 1E-3

    learner = unet_learner(data, models.resnet34,
                           metrics=[m_acc, m_fscore, m_dice], wd=0.1)
    learner.fit_one_cycle(epochs[0], lr_stage_1, pct_start=0.9)

    learner.save('stage-1')
    learner.load('stage-1');

    learner.unfreeze()
    lr_stage_2 = slice(lr_stage_1/400, lr_stage_1/4)

    learner.fit_one_cycle(epochs[1], lr_stage_2, pct_start=0.8)
    learner.save('stage-2')
    learner.load('stage-2');

    learner.freeze()

    learner.export(BASE_PATH/'butterfly.pkl')

    return None


def _check_images(folder_images, folder_labels):
    count = 0
    for (fname_image, fname_label) in zip(
        sorted(folder_images.glob('*.png')),
        sorted(folder_labels.glob('*.png'))
        ):
        if fname_image.stem != fname_label.stem:
            print(f'Jaccuse! {fname_image}')

        image = io.imread(fname_image)
        label = io.imread(fname_label)

        if image.shape[0:2] != label.shape[0:2]:
            print(f'Jaccuse! {fname_image}')
            print(f'Shapes: {image.shape}, {label.shape}')
            count += 1
    print(f'Total: {count} images')
    return None


def _check_weights(weights=None):
    """
    """
    DEFAULT_WEIGHTS = BASE_PATH/'unet_model/unet_butterfly.pkl'

    # checking if the user wants to start training from scratch.
    if weights is None:
        aux_option = input((f'No input weights. Would you like to '
                             'start training from scratch? [Y/N] '))
        if aux_option.upper() == 'N':
            print(f'Using default weights: {DEFAULT_WEIGHTS}.')
            weights = DEFAULT_WEIGHTS
        else:
            return None
    elif type(weights) != 'pathlib.PosixPath':
        weights = Path(weights)

    # file doesn't exist; checking if the user would like to download it.
    if not weights.is_file():
        aux_option = input((f'{weights} not in the path. Would you like to '
                            f'check for {DEFAULT_WEIGHTS}? [Y/N] '))
        if aux_option.upper() == 'Y':
            _fetch_data(filename=DEFAULT_WEIGHTS)
            print('Downloading weights...')
            weights = DEFAULT_WEIGHTS
    # file exists: check if we have the last version; download if not.
    else:
        local_hash = _read_hash_local()
        online_hash = _read_hash_online()
        if local_hash != online_hash:
            print('New training data available. Downloading...')
            _fetch_data(filename=DEFAULT_WEIGHTS)

    return weights


def _fetch_data(filename='unet_butterfly.pkl'):
    """Downloads and checks the hash of unet_butterfly.pkl.

    Parameters
    ----------
    filename : str
        Filename to save the model file.
    """
    FILE_HASH = BASE_PATH/'unet_model/SHA256SUM'

    _download_hash_online(filename=FILE_HASH)
    LOCAL_HASH = _read_hash_local()
    retrieve(url=MODEL_ONLINE, known_hash=f'sha256:{LOCAL_HASH}',
             fname=filename, path='.')

    return None


def _read_hash_local():
    """
    """
    HASH_LOCAL = BASE_PATH/'unet_model/SHA256SUM'

    try:
        with open(HASH_LOCAL, 'r') as file_hash:
            hashes = [line for line in file_hash]
        # expecting only one hash, and not interested in the filename:
        local_hash, _ = hashes[0].split()
    except FileNotFoundError:
        local_hash = None
    return local_hash


def _read_hash_online():
    """
    """
    HASH_ONLINE = BASE_PATH/'unet_model/.SHA256SUM_online'

    _download_hash_online(filename=HASH_ONLINE)
    with open(HASH_ONLINE, 'r') as file_hash:
        hashes = [line for line in file_hash]

    # expecting only one hash, and not interested in the filename:
    online_hash, _ = hashes[0].split()

    return online_hash


def _download_hash_online(filename):
    """
    """
    retrieve(url=HASH_ONLINE, known_hash=None, fname=filename, path='.')
    return None


def _read_training_variables(filename: str) -> Dict[str, int]:
    """Reads train_vars from a JSON file."""
    with open(filename) as file_json:
        train_vars = json.load(file_json)
    expected_keys = ('pad_width',
                     'win_shape',
                     'FOLDER_TRAIN',
                     'folder_dump')

    for key in expected_keys:
        if (key not in train_vars.keys()) or (not train_vars[key]):
            raise RuntimeError(f'{key} is not defined in {filename}.')

    train_vars['win_shape'] = np.array(train_vars['win_shape'])
    return train_vars


def _training_variables() -> Dict[str, int]:
    """Returns variables used previously in the training."""
    train_vars = {
        'win_shape': (608, 608),
        'folder_train': BASE_PATH/'training_images',
        'folder_dump': BASE_PATH/'already_trained_on'
    }
    return train_vars

if __name__ == '__main__':
    main()
